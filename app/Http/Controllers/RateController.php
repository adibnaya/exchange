<?php

namespace App\Http\Controllers;

use App\Rate;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class RateController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::paginate(10);
        return view('rates.index', compact('rates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rate = new Rate();
        $rate->symbol = $request->symbol;
        $rate->rate_to_shekel = $request->rate_to_shekel;
        $rate->save();
        return redirect('rates');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rate = Rate::findOrFail($id);
        return view('rates.show', compact('rate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rate = Rate::findOrFail($id);
        return view('rates.edit', compact('rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rate = Rate::findOrFail($id);
        $rate->update($request->all());
        return redirect('rates');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rate = Rate::findOrFail($id);
        $rate->delete();
        return redirect('rates');
    }

    public function updateAll()
    {
        $endpoint = "https://api.exchangeratesapi.io/latest?base=ILS";
        $client = new Client();
        $response = $client->request('GET', $endpoint);
        $rates = json_decode($response->getBody(), true);
        foreach ($rates['rates'] as $symbol => $rate_to_shekel) {
            $rate_to_shekel = 1 / $rate_to_shekel;
            Rate::updateOrCreate(['symbol' => $symbol], ['rate_to_shekel' => $rate_to_shekel]);
        }
        return redirect('rates');
    }
}
