<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'user_id', 'customer_id', 'sold_id', 'sold_amount', 'bought_id', 'bought_amount', 'commission_percentage',
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }

    public function sold(){
        return $this->belongsTo('App\Rate','sold_id');
    }

    public function bought(){
        return $this->belongsTo('App\Rate','bought_id');
    }
}
