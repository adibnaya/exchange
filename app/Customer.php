<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'country',
    ];

    public function transactions(){
        return $this->hasMany('App\Transaction','customer_id');
    }
}
