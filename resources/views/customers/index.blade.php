@extends('layouts.app')

@section('title', 'Customers')

@section('content')
    @if(Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{Session::get('notallowed')}}
        </div>
    @endif
    <div class="table-header">
        <h1>List of customers</h1>
        <a class="btn btn-primary" href="{{url('/customers/create')}}"> Add new customer</a>
    </div>
    <table class="table">
        <tr>
            <th><a href = "{{url('/customers/sorted/id')}}">id</a></th>
            <th><a href = "{{url('/customers/sorted/name')}}">Name</a></th>
            <th><a href = "{{url('/customers/sorted/email')}}">Email</a></th>
            <th><a href = "{{url('/customers/sorted/address')}}">Address</a></th>
            <th><a href = "{{url('/customers/sorted/country')}}">Country</a></th>
            <th><a href = "{{url('/customers/sorted/phone')}}">Phone</a></th>
            <th>Created</th>
            <th>Updated</th>
        </tr>
        <!-- the table data -->
        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->name}}</td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->address}}</td>
                <td>{{$customer->country}}</td>
                <td>{{$customer->phone}}</td>
                <td>{{$customer->created_at}}</td>
                <td>{{$customer->updated_at}}</td>
                <td>
                    <a href="{{route('customers.edit',$customer->id)}}">Edit</a>
                </td>
                <td>
                    <a href="{{route('customer.delete',$customer->id)}}">Delete</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{$customers->links()}}
@endsection