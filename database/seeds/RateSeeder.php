<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rates')->insert([
            [
                'symbol' => 'ILS',
                'rate_to_shekel' => 1,
            ],
            [
                'symbol' => 'USD',
                'rate_to_shekel' => 3.45,
            ],
            [
                'symbol' => 'THB',
                'rate_to_shekel' => 0.11,
            ],
            [
                'symbol' => 'INR',
                'rate_to_shekel' => 0.047,
            ],
            [
                'symbol' => 'RUB',
                'rate_to_shekel' => 0.044,
            ],
        ]);
    }
}
