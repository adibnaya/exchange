<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->insert([
            [
                'user_id' => 1,
                'customer_id' => 1,
                'sold_id' => 1,
                'sold_amount' => 12,
                'bought_id' => 3,
                'bought_amount' => 33,
                'commission_percentage' => 1,
            ],
            [
                'user_id' => 2,
                'customer_id' => 1,
                'sold_id' => 2,
                'sold_amount' => 2,
                'bought_id' => 1,
                'bought_amount' => 2,
                'commission_percentage' => 2,
            ],
            [
                'user_id' => 3,
                'customer_id' => 1,
                'sold_id' => 3,
                'sold_amount' => 3,
                'bought_id' => 3,
                'bought_amount' => 3,
                'commission_percentage' => 3,
            ],
            [
                'user_id' => 4,
                'customer_id' => 1,
                'sold_id' => 4,
                'sold_amount' => 4,
                'bought_id' => 4,
                'bought_amount' => 4,
                'commission_percentage' => 4,
            ],
            [
                'user_id' => 1,
                'customer_id' => 1,
                'sold_id' => 1,
                'sold_amount' => 12,
                'bought_id' => 3,
                'bought_amount' => 33,
                'commission_percentage' => 1,
            ],
            [
                'user_id' => 2,
                'customer_id' => 1,
                'sold_id' => 2,
                'sold_amount' => 2,
                'bought_id' => 1,
                'bought_amount' => 2,
                'commission_percentage' => 2,
            ],
            [
                'user_id' => 3,
                'customer_id' => 1,
                'sold_id' => 3,
                'sold_amount' => 3,
                'bought_id' => 3,
                'bought_amount' => 3,
                'commission_percentage' => 3,
            ],
            [
                'user_id' => 4,
                'customer_id' => 1,
                'sold_id' => 4,
                'sold_amount' => 4,
                'bought_id' => 4,
                'bought_amount' => 4,
                'commission_percentage' => 4,
            ],
            [
                'user_id' => 1,
                'customer_id' => 1,
                'sold_id' => 1,
                'sold_amount' => 12,
                'bought_id' => 3,
                'bought_amount' => 33,
                'commission_percentage' => 1,
            ],
            [
                'user_id' => 2,
                'customer_id' => 1,
                'sold_id' => 2,
                'sold_amount' => 2,
                'bought_id' => 1,
                'bought_amount' => 2,
                'commission_percentage' => 2,
            ],
            [
                'user_id' => 3,
                'customer_id' => 1,
                'sold_id' => 3,
                'sold_amount' => 3,
                'bought_id' => 3,
                'bought_amount' => 3,
                'commission_percentage' => 3,
            ],
            [
                'user_id' => 4,
                'customer_id' => 1,
                'sold_id' => 4,
                'sold_amount' => 4,
                'bought_id' => 4,
                'bought_amount' => 4,
                'commission_percentage' => 4,
            ],
        ]);
    }
}
