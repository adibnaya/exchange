<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0548391172',
                'country' => 'israel',
                'email' => Str::random(10) . '@gmail.com',
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0589234758',
                'country' => 'usa',
                'email' => Str::random(10) . '@gmail.com',
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0512328764',
                'country' => 'russia',
                'email' => Str::random(10) . '@gmail.com',
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0528333627',
                'country' => 'israel',
                'email' => Str::random(10) . '@gmail.com',
            ],
        ]);
    }
}
