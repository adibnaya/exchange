<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'address' => Str::random(10),
                'phone' => '0548394872',
                'role' => 'admin',
                'email' => 'a@a.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0589384758',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0503928764',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
            [
                'name' => Str::random(10),
                'address' => Str::random(10),
                'phone' => '0528374627',
                'role' => 'user',
                'email' => Str::random(10) . '@gmail.com',
                'password' => Hash::make('12345678'),
            ],
        ]);
    }
}
